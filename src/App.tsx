import Router from './Router';
import { StateContextProvider } from './state/StateProvider';

function App() {
  return (
    <StateContextProvider>
      <Router />
    </StateContextProvider>
  );
}

export default App;
