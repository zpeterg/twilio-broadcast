import styled from 'styled-components';
import ThemeEnum from '../../theme';

export const Input = styled.input`
  padding: 0.7em;
  min-width: 15em;
`;

export const Label = styled.label`
  display: inline-block;
  margin-right: 1em;
  min-width: 12em;
`;

export const Title = styled.h1``;

export const Subtitle = styled.p`
  font-weight: 300;
  opacity: 0.9;
  margin-top: 1em;
  margin-bottom: 2em;
`;

interface ButtonPropType {
  small?: boolean;
  fullWidth?: boolean;
  look?: 'light' | 'normal' | 'ghost';
}

export const Button = styled.button`
  padding: ${({ small }: ButtonPropType) =>
    small ? '0.2em 1.2em' : '0.8em 1.2em'};
  text-transform: uppercase;
  font-size: 1.1em;
  background-color: ${({ look = 'dark' }) =>
    look === 'light'
      ? ThemeEnum.NEUTRAL
      : look === 'ghost'
      ? ThemeEnum.LIGHT
      : ThemeEnum.DARK};
  color: ${({ look = 'dark' }) =>
    look === 'ghost' || look === 'light'
      ? ThemeEnum.DARK
      : ThemeEnum.ACCENT_LIGHT};
  ${({ fullWidth }: ButtonPropType) =>
    fullWidth
      ? `
    width: 100%;
    `
      : `
    margin: 0 0.2em;
    `}
  border: none;
`;
