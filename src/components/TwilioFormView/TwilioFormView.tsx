import { useNavigate } from 'react-router-dom';
import { RouteEnum } from '../../Router';
import { ActionsEnum } from '../../state/actions';
import { useStateContext } from '../../state/StateProvider';
import { TwilioSettingsEnum } from '../../types';
import {
  Label,
  Input,
  Title,
  Subtitle,
  Button,
} from '../FormComponents/FormComponents';
import PageWrap from '../PageWrap/PageWrap';
import useGetNewContactList from '../../hooks/useGetNewContactList';

export default function TwilioFormView({ isIntro }: { isIntro?: boolean }) {
  const {
    state: { twilioSettings },
    dispatch,
  } = useStateContext();
  const navigate = useNavigate();
  const getNewContactList = useGetNewContactList();

  const onChange = (
    twilioSetting: TwilioSettingsEnum,
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    dispatch({
      type: ActionsEnum.ADD_TWILIO,
      twilioSetting,
      content: event.target.value,
    });
  };

  const onNext = () => {
    const contactListId = getNewContactList();
    navigate(
      `${
        isIntro ? RouteEnum.INTRO_CONTACT_FORM : RouteEnum.CONTACT_FORM
      }/${contactListId}`
    );
  };

  return (
    <PageWrap>
      <Title>Twilio Settings</Title>
      <Subtitle>
        {isIntro
          ? "First we need your Twilio settings. These won't leave your browser."
          : 'Input your new settings.'}
      </Subtitle>
      <form onSubmit={(event) => event.preventDefault()}>
        <Label about="service-sid-input">Service SID</Label>
        <Input
          id="service-sid-input"
          value={twilioSettings.serviceSid}
          onChange={(event) => onChange(TwilioSettingsEnum.SERVICE_SID, event)}
        />
        <br />
        <br />
        <Label about="password-input">Password</Label>
        <Input
          id="password-input"
          value={twilioSettings.password}
          onChange={(event) => onChange(TwilioSettingsEnum.PASSWORD, event)}
        />
        <br />
        <br />
        <Label about="message-service-sid">Message Service SID</Label>
        <Input
          id="message-service-sid"
          value={twilioSettings.messageServiceSid}
          onChange={(event) =>
            onChange(TwilioSettingsEnum.MESSAGE_SERVICE_SID, event)
          }
        />
      </form>
      <br />
      <br />
      {isIntro && (
        <Button onClick={onNext} fullWidth>
          Next
        </Button>
      )}
    </PageWrap>
  );
}
