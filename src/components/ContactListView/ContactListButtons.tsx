import { ContactList } from '../../types';
import { Button } from '../FormComponents/FormComponents';
import { useNavigate } from 'react-router-dom';
import { RouteEnum } from '../../Router';
import { Li, Title } from '../GridComponents/GridComponents';
import { useStateContext } from '../../state/StateProvider';
import { ActionsEnum } from '../../state/actions';

export default function ContactListButtons({
  id,
  contactList,
}: {
  id: string;
  contactList: ContactList;
}) {
  const navigate = useNavigate();
  const { dispatch } = useStateContext();

  return (
    <Li>
      <Title>{contactList.name || 'Unnamed'}</Title>
      <Button
        onClick={() => navigate(`${RouteEnum.LIST}/${id}`)}
        small
        look="light"
      >
        Messages
      </Button>
      <Button
        onClick={() => navigate(`${RouteEnum.CONTACT_FORM}/${id}`)}
        small
        look="light"
      >
        Edit
      </Button>
      <Button
        onClick={() => {
          if (
            window.confirm(
              `Are you sure you want to delete ${contactList.name || ''}?`
            )
          ) {
            dispatch({
              type: ActionsEnum.DELETE_CONTACT_LIST,
              contactListId: id,
            });
          }
        }}
        small
        look="light"
      >
        Delete
      </Button>
    </Li>
  );
}
