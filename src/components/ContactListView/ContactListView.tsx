import { useNavigate } from 'react-router-dom';
import useGetNewContactList from '../../hooks/useGetNewContactList';
import { RouteEnum } from '../../Router';
import { useStateContext } from '../../state/StateProvider';
import { Button } from '../FormComponents/FormComponents';
import { Ul } from '../GridComponents/GridComponents';
import IntroRedirect from '../IntroRedirect/IntroRedirect';
import PageWrap from '../PageWrap/PageWrap';
import ContactListButtons from './ContactListButtons';

export default function ContactListView() {
  const {
    state: { contactLists },
  } = useStateContext();
  const getNewContactList = useGetNewContactList();
  const navigate = useNavigate();

  return (
    <PageWrap>
      <Ul>
        {Object.keys(contactLists).map((key) => {
          const contactList = contactLists[key];
          return (
            <ContactListButtons key={key} id={key} contactList={contactList} />
          );
        })}
      </Ul>
      <br />
      <br />
      <Button
        onClick={() => {
          const contactListId = getNewContactList();
          navigate(`${RouteEnum.CONTACT_FORM}/${contactListId}`);
        }}
        fullWidth
      >
        New List
      </Button>
      <br />
      <br />
      <Button
        look="ghost"
        fullWidth
        onClick={() => {
          navigate(RouteEnum.TWILIO_FORM);
        }}
      >
        Twilio Settings
      </Button>
      <IntroRedirect />
    </PageWrap>
  );
}
