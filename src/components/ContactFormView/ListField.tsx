import styled from 'styled-components';

const ListField = styled.textarea`
  width: calc(100% - 6px);
  min-height: 300px;
`;

export default ListField;
