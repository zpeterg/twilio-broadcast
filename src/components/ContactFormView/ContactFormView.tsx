import { useNavigate, useParams } from 'react-router-dom';
import { RouteEnum, RouteParamEnum } from '../../Router';
import { ActionsEnum } from '../../state/actions';
import { useStateContext } from '../../state/StateProvider';
import { ContactLists, ContactType } from '../../types';
import {
  Button,
  Input,
  Subtitle,
  Title,
} from '../FormComponents/FormComponents';
import PageWrap from '../PageWrap/PageWrap';
import ListField from './ListField';

export default function ContactFormView({ isIntro }: { isIntro?: boolean }) {
  const navigate = useNavigate();
  const params = useParams();

  const contactListId = params[
    RouteParamEnum.CONTACT_LIST_ID
  ] as keyof ContactLists;
  const { state, dispatch } = useStateContext();

  const currentContactList = state.contactLists[contactListId];

  return (
    <PageWrap>
      <Title>Contact List</Title>
      <Subtitle>
        {isIntro
          ? "Now let's get you started with your first contact list." +
            " You'll use this later for sending out sets of text messages."
          : 'Create a new contact list'}
      </Subtitle>
      <Input
        value={currentContactList.name}
        onChange={(event) => {
          event.preventDefault();
          dispatch({
            type: ActionsEnum.SET_CONTACT_LIST,
            contactListId,
            contactList: { ...currentContactList, name: event.target.value },
          });
        }}
      />
      <ListField
        defaultValue={currentContactList.contacts
          .map(({ name, phoneNumber }) => `${name}, ${phoneNumber}`)
          .join('\n')}
        onBlur={(event) => {
          event.preventDefault();
          dispatch({
            type: ActionsEnum.SET_CONTACT_LIST,
            contactListId,
            contactList: {
              ...currentContactList,
              contacts: event.target.value
                .split('\n')
                .map((line: string): ContactType => {
                  const word = line.split(',').map((word) => word.trim());
                  return {
                    name: word[0],
                    phoneNumber: word[1] || word[0],
                    messageInstances: [],
                  };
                }),
            },
          });
        }}
      />
      <br />
      <br />
      <Button onClick={() => navigate(RouteEnum.SEND)} fullWidth>
        All Done
      </Button>
    </PageWrap>
  );
}
