import React from 'react';
import { useNavigate } from 'react-router-dom';
import useGetNewContactList from '../../hooks/useGetNewContactList';
import { RouteEnum } from '../../Router';
import { useStateContext } from '../../state/StateProvider';

export default function IntroRedirect() {
  const { state } = useStateContext();
  const navigate = useNavigate();
  const { dispatch } = useStateContext();
  const getNewContactList = useGetNewContactList();

  React.useEffect(() => {
    if (!state.twilioSettings.username) {
      navigate(RouteEnum.INTRO_TWILIO_FORM, { replace: true });
    } else if (Object.keys(state.contactLists).length < 1) {
      const contactListId = getNewContactList();
      navigate(`${RouteEnum.INTRO_CONTACT_FORM}/${contactListId}`, {
        replace: true,
      });
    }
  }, [
    dispatch,
    getNewContactList,
    state.contactLists,
    state.twilioSettings,
    navigate,
  ]);

  return null;
}
