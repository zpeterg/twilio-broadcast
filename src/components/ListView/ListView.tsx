import { useNavigate, useParams } from 'react-router-dom';
import useGetNewMessage from '../../hooks/useGetNewMessage';
import { RouteEnum, RouteParamEnum } from '../../Router';
import { useStateContext } from '../../state/StateProvider';
import { Button, Title } from '../FormComponents/FormComponents';
import { Ul } from '../GridComponents/GridComponents';
import PageWrap from '../PageWrap/PageWrap';
import ListButtons from './ListButtons';

export default function ListView() {
  const params = useParams();
  const currentContactListId = params[RouteParamEnum.CONTACT_LIST_ID];
  const currentMessageId = params[RouteParamEnum.MESSAGE_ID];
  const getNewMessage = useGetNewMessage({
    contactListId: currentContactListId,
  });
  const navigate = useNavigate();
  const {
    state: { contactLists },
  } = useStateContext();

  const currentContactList =
    currentContactListId && contactLists[currentContactListId];

  return (
    <PageWrap>
      {!currentContactList ? (
        'Contact list not found'
      ) : (
        <>
          <Title>{currentContactList.name}</Title>

          <Ul>
            {currentContactList.messages.map((message) => (
              <ListButtons
                key={message.id}
                message={message}
                contactListId={currentContactListId}
                isSelected={message.id === currentMessageId}
              />
            ))}
          </Ul>
          <Button
            fullWidth
            onClick={async () => {
              const newMessageId = getNewMessage();
              if (newMessageId)
                navigate(
                  `${RouteEnum.LIST}/${currentContactListId}/${newMessageId}`
                );
            }}
          >
            Add Message
          </Button>
          <Button
            fullWidth
            look="ghost"
            onClick={() => navigate(RouteEnum.SEND)}
          >
            Back
          </Button>
        </>
      )}
    </PageWrap>
  );
}
