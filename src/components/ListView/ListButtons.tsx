import { ActionsEnum } from '../../state/actions';
import { useStateContext } from '../../state/StateProvider';
import { ContactLists, MessageType } from '../../types';
import { Button, Input } from '../FormComponents/FormComponents';
import { Li, Title } from '../GridComponents/GridComponents';
import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';
import { useNavigate } from 'react-router-dom';
import { RouteEnum } from '../../Router';
import styled from 'styled-components';
dayjs.extend(utc);

const RowTitle = styled(Title)`
  flex: 1;
`;

const Wrap = styled.div`
  width: 100%;
`;
const RowWrap = styled.div`
  display: flex;
  width: 100%;
`;

const SubMenu = styled.div`
  display: flex;
  flex-direction: column;
  width: calc(100% - 100px);
  margin: 50px;
`;

export default function ListButtons({
  message: { id, text, sendAt, wasSent },
  contactListId,
  isSelected,
}: {
  message: MessageType;
  contactListId: keyof ContactLists;
  isSelected: boolean;
}) {
  const { dispatch } = useStateContext();
  const navigate = useNavigate();

  const displayTime = sendAt.local().format('MMM D, h:mm A');
  return (
    <Li>
      <Wrap>
        <RowWrap>
          <RowTitle>
            {displayTime} - {text.substring(0, 20)}
          </RowTitle>
          {wasSent && 'Sent'}
          {!isSelected && !wasSent && (
            <>
              <Button
                onClick={() =>
                  navigate(`${RouteEnum.LIST}/${contactListId}/${id}`)
                }
              >
                Edit
              </Button>
              <Button
                onClick={() => {
                  if (
                    !window.confirm(
                      `Are you sure that you want to delete message for ${displayTime}`
                    )
                  )
                    return;
                  dispatch({
                    type: ActionsEnum.DELETE_MESSAGE,
                    messageId: id,
                    contactListId,
                  });
                }}
              >
                Delete
              </Button>
            </>
          )}
        </RowWrap>
        {isSelected && (
          <SubMenu>
            <Input
              type="date"
              value={sendAt.local().format('YYYY-MM-DD')}
              onChange={(event) => {
                event.preventDefault();
                const newSendAt = dayjs(
                  event.target.value + sendAt.local().format('THH:mm:ss')
                );
                if (newSendAt.isValid() && contactListId) {
                  dispatch({
                    type: ActionsEnum.EDIT_MESSAGE_SEND_AT,
                    messageId: id,
                    contactListId,
                    sendAt: newSendAt.utc(),
                  });
                }
              }}
            />
            <Input
              type="time"
              value={sendAt.local().format('HH:mm')}
              onChange={(event) => {
                event.preventDefault();
                const newSendAt = dayjs(
                  sendAt.local().format('YYYY-MM-DDT') + event.target.value
                );
                if (newSendAt.isValid() && contactListId) {
                  dispatch({
                    type: ActionsEnum.EDIT_MESSAGE_SEND_AT,
                    messageId: id,
                    contactListId,
                    sendAt: newSendAt.utc(),
                  });
                }
              }}
            />
            <br />
            Use an @ symbol to inject a name into the message.
            <Input
              value={text}
              onChange={(event) => {
                event.preventDefault();
                if (contactListId) {
                  dispatch({
                    type: ActionsEnum.EDIT_MESSAGE_TEXT,
                    messageId: id,
                    contactListId,
                    text: event.target.value,
                  });
                }
              }}
            />
            <br />
            <br />
            <Button
              onClick={() => navigate(`${RouteEnum.LIST}/${contactListId}`)}
            >
              Done
            </Button>
          </SubMenu>
        )}
      </Wrap>
    </Li>
  );
}
