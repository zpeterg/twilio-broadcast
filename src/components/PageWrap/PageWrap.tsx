import styled from 'styled-components';

const Wrap = styled.div`
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
`;

const Inner = styled.div`
  max-width: 800px;
  min-width: 200px;
  margin: 20px;
`;

export default function PageWrap({ children }: { children: React.ReactNode }) {
  return (
    <Wrap>
      <Inner>{children}</Inner>
    </Wrap>
  );
}
