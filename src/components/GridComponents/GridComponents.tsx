import styled from 'styled-components';

export const Ul = styled.ul`
  padding: 0;
`;

export const Title = styled.h2`
  margin: 0.3em 2em 0.3em 0;
  min-width: 200px;
  font-size: 1.5em;
`;

export const Li = styled.li`
  width: 100%;
  display: flex;
  margin: 0.5em 0;
`;
