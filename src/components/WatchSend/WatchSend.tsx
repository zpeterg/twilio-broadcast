import React from 'react';
import { useStateContext } from '../../state/StateProvider';
import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';
import useSendSms, { SendSmsResponseType } from '../../hooks/useSendSms';
import { ActionsEnum } from '../../state/actions';
dayjs.extend(utc);

export default function WatchSend() {
  const {
    state: { contactLists },
    dispatch,
  } = useStateContext();
  const sendSms = useSendSms();

  const send = React.useCallback(() => {
    const promises: Promise<SendSmsResponseType>[] = Object.keys(
      contactLists
    ).reduce((cumm, thisContactListId) => {
      const contactList = contactLists[thisContactListId];
      const messagesToSend = contactList.messages.filter(
        ({ wasSent, sendAt }) => {
          return !wasSent && sendAt.isBefore(dayjs.utc());
        }
      );
      messagesToSend.forEach(({ id, text }) =>
        contactList.contacts.forEach(
          ({ phoneNumber, name }) =>
            (cumm = [
              ...cumm,
              sendSms({
                messageId: id,
                contactListId: thisContactListId,
                phoneNumber,
                text: text.replaceAll('@', name),
              }),
            ])
        )
      );
      return cumm;
    }, [] as Promise<SendSmsResponseType>[]);
    Promise.all(promises).then((res) => {
      res.forEach((thisRes) => {
        if ('error' in thisRes)
          return console.error('Error while sending: ' + thisRes.error);

        dispatch({
          type: ActionsEnum.SET_MESSAGES_AS_SENT,
          messageToMarkSent: thisRes.content,
        });
      });
    });
  }, [contactLists, sendSms, dispatch]);

  React.useEffect(() => {
    const timeout = setInterval(send, 60_000);
    send();
    return () => clearInterval(timeout);
  }, [send]);

  return null;
}
