import { HashRouter, Route, Routes } from 'react-router-dom';
import ContactFormView from './components/ContactFormView/ContactFormView';
import ContactListView from './components/ContactListView/ContactListView';
import ListView from './components/ListView/ListView';
import TwilioFormView from './components/TwilioFormView/TwilioFormView';
import WatchSend from './components/WatchSend/WatchSend';

export enum RouteEnum {
  SEND = '/',
  TWILIO_FORM = '/twilio-form',
  CONTACT_FORM = '/contact-form',
  INTRO_TWILIO_FORM = '/intro/twilio-form',
  INTRO_CONTACT_FORM = '/intro/contact-form',
  LIST = '/list',
}

export enum RouteParamEnum {
  CONTACT_LIST_ID = 'contactListId',
  MESSAGE_ID = 'messageId',
}

export default function Router() {
  return (
    <HashRouter>
      <Routes>
        <Route path={RouteEnum.SEND} element={<ContactListView />} />
        <Route
          path={RouteEnum.INTRO_TWILIO_FORM}
          element={<TwilioFormView isIntro />}
        />
        <Route
          path={`${RouteEnum.INTRO_CONTACT_FORM}/:${RouteParamEnum.CONTACT_LIST_ID}`}
          element={<ContactFormView isIntro />}
        />
        <Route path={RouteEnum.TWILIO_FORM} element={<TwilioFormView />} />
        <Route
          path={`${RouteEnum.CONTACT_FORM}/:${RouteParamEnum.CONTACT_LIST_ID}`}
          element={<ContactFormView />}
        />
        <Route
          path={`${RouteEnum.LIST}/:${RouteParamEnum.CONTACT_LIST_ID}/:${RouteParamEnum.MESSAGE_ID}`}
          element={<ListView />}
        />
        <Route
          path={`${RouteEnum.LIST}/:${RouteParamEnum.CONTACT_LIST_ID}`}
          element={<ListView />}
        />
      </Routes>
      <WatchSend />
    </HashRouter>
  );
}
