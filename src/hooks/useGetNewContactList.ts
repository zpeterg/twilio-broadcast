import React from 'react';
import { uuid } from 'uuidv4';
import { ActionsEnum } from '../state/actions';
import { useStateContext } from '../state/StateProvider';

export default function useGetNewContactList(): () => string {
  const { dispatch } = useStateContext();
  return React.useCallback(() => {
    const contactListId = uuid();
    dispatch({
      type: ActionsEnum.SET_CONTACT_LIST,
      contactListId,
      contactList: { name: '', contacts: [], messages: [] },
    });
    return contactListId;
  }, [dispatch]);
}
