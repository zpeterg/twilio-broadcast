import React from 'react';
import { uuid } from 'uuidv4';
import { ActionsEnum } from '../state/actions';
import { useStateContext } from '../state/StateProvider';

export default function useGetNewMessage({
  contactListId,
}: {
  contactListId?: string;
}) {
  const { dispatch } = useStateContext();
  return React.useCallback(() => {
    if (!contactListId)
      return void console.error('No contact list id available');
    const messageId = uuid();
    dispatch({ type: ActionsEnum.ADD_MESSAGE, messageId, contactListId });
    return messageId;
  }, [dispatch, contactListId]);
}
