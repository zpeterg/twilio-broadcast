import React from 'react';
import { useStateContext } from '../state/StateProvider';
import axios, { AxiosResponse } from 'axios';
import { MessageToMarkSentType } from '../types';

export interface SendSmsInterface {
  messageId: string;
  contactListId: string;
  phoneNumber: string;
  text: string;
}

export type SendSmsResponseType =
  | {
      content: MessageToMarkSentType;
    }
  | { error: string };

type TwilioResponseType = { sid: string; status: 'accepted' | 'other' };

export default function useSendSms() {
  const {
    state: { twilioSettings },
  } = useStateContext();

  return React.useCallback(
    async ({
      contactListId,
      messageId,
      phoneNumber,
      text,
    }: SendSmsInterface): Promise<SendSmsResponseType> => {
      const params = new URLSearchParams();
      params.append('To', phoneNumber);
      params.append('MessagingServiceSid', twilioSettings.messageServiceSid);
      params.append('Body', text);
      params.append('ProvideFeedback', 'true');
      const config = {
        auth: {
          username: twilioSettings.serviceSid,
          password: twilioSettings.password,
        },
      };
      let res;
      try {
        res = (await axios.post(
          `https://api.twilio.com/2010-04-01/Accounts/${twilioSettings.serviceSid}/Messages.json`,
          params,
          config
        )) as AxiosResponse<TwilioResponseType>;
      } catch (error) {
        console.error('Error while sending', error);
        return { error: 'Error sending: ' + error };
      }

      if (!res || res.status !== 201 || res.data.status !== 'accepted') {
        return {
          error: `Error in sending response: ${res.status}, ${res.data.status}`,
        };
      }

      return { content: { contactListId, messageId, twilioSid: res.data.sid } };
    },
    [twilioSettings]
  );
}
