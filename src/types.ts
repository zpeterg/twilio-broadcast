import { Dayjs } from 'dayjs';

export enum TwilioSettingsEnum {
  USERNAME = 'username',
  PASSWORD = 'password',
  SERVICE_SID = 'serviceSid',
  MESSAGE_SERVICE_SID = 'messageServiceSid',
}

export type TwilioSettingsType = {
  [TwilioSettingsEnum.USERNAME]: string;
  [TwilioSettingsEnum.PASSWORD]: string;
  [TwilioSettingsEnum.SERVICE_SID]: string;
  [TwilioSettingsEnum.MESSAGE_SERVICE_SID]: string;
};

export type MessageType = {
  id: string;
  text: string;
  sendAt: Dayjs;
  wasSent: boolean;
};

export type MessageInstanceType = {
  id: string;
  messageId: string;
  sentAt: Dayjs;
};

export type ContactType = {
  name: string;
  phoneNumber: string;
  messageInstances: MessageInstanceType[];
};

export type ContactList = {
  name: string;
  contacts: ContactType[];
  messages: MessageType[];
};

export type ContactLists = { [key: string]: ContactList };

export type StateType = {
  twilioSettings: TwilioSettingsType;
  contactLists: ContactLists;
};

export type MessageToMarkSentType = {
  contactListId: string;
  messageId: string;
  twilioSid: string;
};
