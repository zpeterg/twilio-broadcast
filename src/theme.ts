enum ThemeEnum {
  DARK = '#424B54',
  LIGHT = '#FFFFFF',
  NEUTRAL = '#C5BAAF',
  ACCENT_LIGHT = '#EBCFB2',
  ACCENT = '#E1CE7A',
}

export default ThemeEnum;
