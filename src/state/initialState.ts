import { StateType } from '../types';

const initialState: StateType = {
  contactLists: {},
  twilioSettings: {
    password: '',
    username: '',
    serviceSid: '',
    messageServiceSid: '',
  },
};

export default initialState;
