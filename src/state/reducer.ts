import { StateType } from '../types';
import { ActionsEnum, ActionType } from './actions';
import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';
dayjs.extend(utc);

const reducer = (state: StateType, action: ActionType) => {
  const newState = { ...state };

  switch (action.type) {
    case ActionsEnum.ADD_TWILIO:
      newState.twilioSettings = {
        ...newState.twilioSettings,
        [action.twilioSetting]: action.content,
      };
      break;
    case ActionsEnum.SET_CONTACT_LIST:
      action.contactList.contacts = action.contactList.contacts.filter(
        ({ phoneNumber }) => !!phoneNumber
      );
      newState.contactLists[action.contactListId] = action.contactList;
      break;
    case ActionsEnum.DELETE_CONTACT_LIST:
      delete newState.contactLists[action.contactListId];
      break;
    case ActionsEnum.ADD_MESSAGE:
      (() => {
        console.log('adding');
        const currentContactList = newState.contactLists[action.contactListId];
        const newMessage = {
          id: action.messageId,
          text: 'Enter message text here',
          sendAt: dayjs.utc().add(1, 'day'),
          wasSent: false,
        };
        newState.contactLists[action.contactListId] = {
          ...currentContactList,
          messages: [...currentContactList.messages, newMessage],
        };
      })();
      break;
    case ActionsEnum.EDIT_MESSAGE_SEND_AT:
      (() => {
        const messageI = newState.contactLists[
          action.contactListId
        ].messages.findIndex(({ id }) => id === action.messageId);
        newState.contactLists[action.contactListId].messages[messageI].sendAt =
          action.sendAt;
      })();
      break;
    case ActionsEnum.EDIT_MESSAGE_TEXT:
      (() => {
        const messageI = newState.contactLists[
          action.contactListId
        ].messages.findIndex(({ id }) => id === action.messageId);
        newState.contactLists[action.contactListId].messages[messageI].text =
          action.text;
      })();
      break;
    case ActionsEnum.SET_MESSAGES_AS_SENT:
      (() => {
        const { contactListId, messageId, twilioSid } =
          action.messageToMarkSent;
        // Mark message as sent
        newState.contactLists[contactListId].messages = newState.contactLists[
          contactListId
        ].messages.map((message) => ({
          ...message,
          wasSent: message.id === messageId ? true : message.wasSent,
        }));
        // Add to contact -> instance
        newState.contactLists[contactListId].contacts.map((contact) => ({
          ...contact,
          messageInstances: [
            ...contact.messageInstances,
            { id: twilioSid, messageId, sentAt: dayjs.utc() },
          ],
        }));
      })();
      break;
    case ActionsEnum.DELETE_MESSAGE:
      (() => {
        newState.contactLists[action.contactListId].messages =
          newState.contactLists[action.contactListId].messages.filter(
            ({ id }) => id !== action.messageId
          );
      })();
      break;
    default:
    // pass
  }

  console.log(newState);
  return newState;
};

export default reducer;
