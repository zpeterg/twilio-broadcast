import { Dayjs } from 'dayjs';
import {
  ContactList,
  ContactLists,
  MessageToMarkSentType,
  TwilioSettingsEnum,
} from '../types';

export enum ActionsEnum {
  ADD_TWILIO = 'add-twilio',
  SET_CONTACT_LIST = 'set-contact-list',
  DELETE_CONTACT_LIST = 'delete-contact-lits',
  ADD_MESSAGE = 'add-message',
  EDIT_MESSAGE_SEND_AT = 'edit-message-send-at',
  EDIT_MESSAGE_TEXT = 'edit-message-text',
  SET_MESSAGES_AS_SENT = 'set-messages-as-sent',
  DELETE_MESSAGE = 'delete-message',
}

export type ActionType =
  | {
      type: ActionsEnum.ADD_TWILIO;
      twilioSetting: TwilioSettingsEnum;
      content: string;
    }
  | {
      type: ActionsEnum.SET_CONTACT_LIST;
      contactListId: keyof ContactLists;
      contactList: ContactList;
    }
  | {
      type: ActionsEnum.DELETE_CONTACT_LIST;
      contactListId: keyof ContactLists;
    }
  | {
      type: ActionsEnum.ADD_MESSAGE;
      messageId: string;
      contactListId: keyof ContactLists;
    }
  | {
      type: ActionsEnum.EDIT_MESSAGE_SEND_AT;
      contactListId: keyof ContactLists;
      messageId: string;
      sendAt: Dayjs;
    }
  | {
      type: ActionsEnum.EDIT_MESSAGE_TEXT;
      contactListId: keyof ContactLists;
      messageId: string;
      text: string;
    }
  | {
      type: ActionsEnum.SET_MESSAGES_AS_SENT;
      messageToMarkSent: MessageToMarkSentType;
    }
  | {
      type: ActionsEnum.DELETE_MESSAGE;
      contactListId: keyof ContactLists;
      messageId: string;
    };
