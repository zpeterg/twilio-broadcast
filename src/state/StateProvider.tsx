import React from 'react';
import { StateType } from '../types';
import { ActionType } from './actions';
import initialState from './initialState';
import reducer from './reducer';
import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';
dayjs.extend(utc);

const StateContext = React.createContext<{
  state: StateType;
  dispatch: React.Dispatch<ActionType>;
}>({
  state: initialState,
  dispatch: () => console.error('DISPATCH IS NOT SETUP'),
});

function getStoredSettings(): StateType {
  let res: StateType = initialState;
  try {
    res = JSON.parse(localStorage.getItem('state') || '');
  } catch (e) {
    console.error('Could not restore state from local storage: ' + e);
  }
  Object.keys(res.contactLists).forEach((key) => {
    const thisContactList = res.contactLists[key];

    res.contactLists[key].messages = thisContactList.messages.map((message) => {
      message.sendAt = dayjs.utc(message.sendAt);
      return message;
    });
  });

  return res;
}

function setStoredState(state: StateType): void {
  localStorage.setItem('state', JSON.stringify(state));
}

export function StateContextProvider({
  children,
}: {
  children: React.ReactNode;
}) {
  const [state, dispatch] = React.useReducer(reducer, getStoredSettings());

  React.useEffect(() => {
    setStoredState(state);
  }, [state]);

  return (
    <StateContext.Provider value={{ state, dispatch }}>
      {children}
    </StateContext.Provider>
  );
}

export function useStateContext() {
  return React.useContext(StateContext);
}
